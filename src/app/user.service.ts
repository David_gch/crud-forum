import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { User } from './user';
import { catchError, map } from 'rxjs/operators';
import { TranslateService } from 'ng2-translate';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private JSONplaceholderURL = 'https://jsonplaceholder.typicode.com/users';
  private users$: Observable<User[]>;

  // Searcher
  private buscadorString = new BehaviorSubject('');
  buscador = this.buscadorString.asObservable();

  constructor(private http: HttpClient, private translate: TranslateService) {
    this.baseData();
  }

  changeLanguage(language: string) {
    this.translate.use(language);
  }

  startSearch(search: string) {
    this.buscadorString.next(search);
  }

  baseData() {
    if (this.users$ == null) {
      this.users$ = this.http.get<User[]>(this.JSONplaceholderURL)
      .pipe(
        catchError(this.handleError('getUsers', []))
      );
    }
  }

  getUsers(): Observable<User[]> {
    return this.users$;
  }

  getUsersByName(name: string): Observable<User[]> {
    return this.users$.pipe(
      map(results => results.filter(filter => filter.name === name))
    );
  }

  addUser(user: User): Observable<User> {
   return this.http.post<User>(this.JSONplaceholderURL, user, httpOptions);
  }

  saveData(users: User[]) {
    this.users$ = of(users);
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.error(error);

      return of(result as T);
    };
  }
}
