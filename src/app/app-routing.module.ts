import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UsersComponent } from './users/users.component';
import { PostComponent } from './post/post.component';
import { CommentsComponent } from './comments/comments.component';

import { AuthGuard } from './auth-guard.service';


const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', canActivateChild: [AuthGuard], loadChildren: './login/login.module#LoginModule' },
  { path: 'users', canActivate: [AuthGuard], component: UsersComponent },
  { path: 'post/:id', canActivate: [AuthGuard], component: PostComponent },
  { path: 'comments/:id', canActivate: [AuthGuard],  component: CommentsComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
