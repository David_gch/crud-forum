import { Component, OnInit } from '@angular/core';
import { TranslateService } from 'ng2-translate';
import { SpinnerService } from './spinner/spinner.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Forum';
  load: boolean;

  constructor(private translate: TranslateService, private spinnerService: SpinnerService) {
    translate.setDefaultLang('es');
    translate.use('es');
  }

  ngOnInit() {
    this.spinnerService.getLoadState().subscribe(loader => this.load = loader);
  }
}
