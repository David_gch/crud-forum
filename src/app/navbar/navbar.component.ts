import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { LoginService } from '../login/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  nameSearch: string;

  constructor(private userService: UserService, public loginService: LoginService, public router: Router) {}

  ngOnInit() {}

  search() {
    this.userService.startSearch(this.nameSearch);
  }

  changeLanguage(language: string) {
    this.userService.changeLanguage(language);
  }

  closeSession(): void {
    this.loginService.closeSession();
    this.router.navigate(['/login']);
  }
}
