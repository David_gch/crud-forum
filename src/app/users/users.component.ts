import { Component, OnInit, TemplateRef } from '@angular/core';
import { User } from '../user';
import { UserService } from '../user.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { SpinnerService } from '../spinner/spinner.service';
import { finalize, tap } from 'rxjs/operators';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  users: User[] = [];
  nameSearch: string;
  modalUserRef: BsModalRef;

  constructor(private userService: UserService, private modalService: BsModalService, private spinnerService: SpinnerService) {
    this.userService.buscador.subscribe(name => this.nameSearch = name);
  }

  ngOnInit() {
    setTimeout(() => this.getUsers());
  }

  createUser(template: TemplateRef<any>) {
    this.modalUserRef = this.modalService.show(template);
  }

  getUsers(): void {
    this.userService.getUsers().pipe(
      tap(() => this.spinnerService.setLoadState(true)),
      delay(1000),
      finalize(() => this.spinnerService.setLoadState(false))
    ).subscribe(userObservable => this.users = userObservable);
  }

  addUser(name: string): void {
    const user = new User;
    name = name.trim();
    if (!name) { return; }

    user.id = this.users.length + 1;
    user.name = name;
    user.password = 'password';

    this.userService.addUser(user).subscribe(newUser => this.users.push(newUser));
    this.userService.saveData(this.users);
  }
}
