import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { PostComponent } from './post/post.component';
import { CommentsComponent } from './comments/comments.component';
import { AppRoutingModule } from './app-routing.module';
import { NavbarComponent } from './navbar/navbar.component';
import { FilterPipe } from './filter.pipe';
import { TranslateModule, TranslateLoader, TranslateStaticLoader } from 'ng2-translate';
import { ModalModule } from 'ngx-bootstrap/modal';
import { Http } from '@angular/http';
import { AuthGuard } from './auth-guard.service';
import { LoginService } from './login/login.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { SpinnerComponent } from './spinner/spinner.component';
import { SpinnerService } from './spinner/spinner.service';
import { ModalContentComponent } from './modal/modal.component';
import { UserService } from './user.service';
import { SharedModule } from './modal/shared.module';

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    PostComponent,
    CommentsComponent,
    NavbarComponent,
    FilterPipe,
    SpinnerComponent,
    ModalContentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ModalModule.forRoot(),
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    FormsModule,
    TranslateModule.forRoot({
      provide: TranslateLoader,
      useFactory: (http: Http) => new TranslateStaticLoader(http, '../assets/traductor', '.json'),
      deps: [Http]
    }),
    SharedModule
  ],
  providers: [ AuthGuard, LoginService, SpinnerService, UserService ],
  bootstrap: [ AppComponent ],
  exports: [ TranslateModule ],
  entryComponents: [ ModalContentComponent ]
})
export class AppModule { }
