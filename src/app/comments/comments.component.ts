import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Comment } from '../comment';
import { CommentService } from '../comment.service';
import { Location } from '@angular/common';
import { SpinnerService } from '../spinner/spinner.service';
import { finalize, tap } from 'rxjs/operators';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css']
})
export class CommentsComponent implements OnInit {
  comments: Comment[] = [];
  loading: boolean;

  constructor(private route: ActivatedRoute, private commentService: CommentService,
    private location: Location, private spinnerService: SpinnerService) { }

  ngOnInit() {
    setTimeout(() => this.getComments());
    this.spinnerService.setLoadState(true);
    this.loading = true;
  }

  getComments(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.commentService.getComments(id).pipe(
      delay(1000),
      finalize(() => {
        this.spinnerService.setLoadState(false);
        this.loading = false;
      })
    ).subscribe(commentObservable =>  this.comments = commentObservable);
  }

  goBack(): void {
    this.location.back();
  }
}
