import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from 'ng2-translate';

const ToastOptions = {
  positionClass: 'toast-bottom-center',
  timeOut: 2000
};

@Injectable({
  providedIn: 'root'
})
export class ToastService {
  lang: string;

  constructor(private toastr: ToastrService, private translate: TranslateService) { }

  toastAddUser() {
    const lang = this.translate.currentLang;
    if (lang === 'es') {
      this.toastr.success('Usuario añadido', '', ToastOptions);
    } else {
      this.toastr.success('User added', '', ToastOptions);
    }
  }

  toastDeniedAccess() {
    this.lang = this.translate.currentLang;
    if (this.lang === 'es') {
      this.toastr.error('Inicie sesión para poder acceder', '', ToastOptions);
    } else {
      this.toastr.error('Sign in to access', '', ToastOptions);
    }
  }

  toastUserNotFound() {
    this.lang = this.translate.currentLang;
    if (this.lang === 'es') {
      this.toastr.error('Usuario no encontrado', '', ToastOptions);
    } else {
      this.toastr.error('User not found', '', ToastOptions);
    }
  }

  toastUserExists() {
    this.lang = this.translate.currentLang;
    if (this.lang === 'es') {
      this.toastr.error('El nombre de usuario ya existe', '', ToastOptions);
    } else {
      this.toastr.error('The username already exists', '', ToastOptions);
    }
  }
}
