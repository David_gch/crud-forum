import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Post } from '../post';
import { PostService } from '../post.service';
import { Location } from '@angular/common';
import { SpinnerService } from '../spinner/spinner.service';
import { finalize, tap } from 'rxjs/operators';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
  posts: Post[] = [];
  loading: boolean;

  constructor(private route: ActivatedRoute, private postService: PostService,
    private location: Location, private spinnerService: SpinnerService) {}

  ngOnInit() {
    setTimeout(() => this.getPosts());
    this.spinnerService.setLoadState(true);
    this.loading = true;
  }

  getPosts(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.postService.getPosts(id).pipe(
      delay(1000),
      finalize(() => {
        this.spinnerService.setLoadState(false);
        this.loading = false;
      })
    ).subscribe(postObservable => this.posts = postObservable);
  }

  goBack(): void {
    this.location.back();
  }

}
