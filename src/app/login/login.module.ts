import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginComponent } from './login.component';
import { LoginInterfaceComponent } from './login-interface.component';
import { LoginRoutingModule } from './login-routing.module';
import { TranslateModule } from 'ng2-translate';
import { SharedModule } from '../modal/shared.module';

@NgModule({
  imports: [
    CommonModule,
    LoginRoutingModule,
    TranslateModule,
    SharedModule
  ],
  declarations: [ LoginComponent, LoginInterfaceComponent ]
})
export class LoginModule { }
