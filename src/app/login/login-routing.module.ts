import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login.component';
import { LoginInterfaceComponent } from './login-interface.component';

const routes: Routes = [
  { path: '', component: LoginComponent, children: [
    { path: '', component: LoginInterfaceComponent }
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule { }
