import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import { UserService } from '../user.service';
import { LoginService } from './login.service';
import { Router } from '@angular/router';
import { ToastService } from '../toast.service';

@Component({
  selector: 'app-login-interface',
  templateUrl: './login-interface.component.html',
  styleUrls: ['./login-interface.component.css']
})
export class LoginInterfaceComponent implements OnInit {
  users: User[];
  userFind: User;

  constructor(private loginService: LoginService, private userService: UserService, private router: Router,
    private toastService: ToastService) {
      this.userService.getUsers()
      .subscribe(userObservable => {
        this.users = userObservable;
      });
  }

  ngOnInit() {
  }

  sessionStart(name: string, password: string): void {
    // Subscribe no esta funcionando como debería
    this.userService.getUsers()
    .subscribe(userObservable => {
      this.users = userObservable;
    });
    this.userFind = this.users.find( user => user.name === name && user.password === password);
    if (this.userFind !== undefined) {
      this.loginService.startSession(this.userFind);
      this.router.navigate(['/users']);
    } else {
      this.toastService.toastUserNotFound();
      this.userFind = undefined;
    }
  }
}
