import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { User } from '../user';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  userSession: User;

  constructor() { }

  startSession(user: User): void {
    this.userSession = user;
  }

  closeSession(): void {
    this.userSession = undefined;
  }

  getSession(): User {
    return this.userSession;
  }

  isLogged(): boolean {
    if (this.userSession !== undefined) {
      return true;
    }
    return false;
  }

  isAlreadyLogged(): boolean {
    if (this.userSession !== undefined) {
      return false;
    }
    return true;
  }
}
