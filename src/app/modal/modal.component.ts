import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { User } from '../user';
import { ToastService } from '../toast.service';
import { UserService } from '../user.service';

@Component({
  selector: 'app-modal-content',
  template: `
  <div class="modal-header">
    <h4 translate class="modal-title pull-left">Modal.Title</h4>
    <button type="button" class="close pull-right" aria-label="Close" (click)="bsModalRef.hide()">
    <span aria-hidden="true">&times;</span>
    </button>
    </div>

    <div class="modal-body">
    <table>
    <tr>
      <th translate>Users.Name</th>
      <th><input #userNameModal /></th>
    </tr>
    <tr>
      <th translate>Users.Password</th>
      <th>
        <input type="password" #userPasswordModal />
      </th>
    </tr>
    </table>

    <button translate class="center-element" (click)="addUser(userNameModal.value, userPasswordModal.value)">
    Button.AddUser
    </button>

    <div>
    <button translate class="modalButton btn btn-primary" (click)="bsModalRef.hide()">Button.Back</button>
    </div>
  </div>
  `
})
export class ModalContentComponent {
  users: User[];
  userFind: User;
  modalUserRef: BsModalRef;

  constructor(public bsModalRef: BsModalRef, private userService: UserService, private toastService: ToastService
  , private modalService: BsModalService) {
    this.userService.getUsers().subscribe(users => this.users = users);
    this.modalUserRef = bsModalRef;
  }

  addUser(name: string, password: string): void {
    const user = new User;
    name = name.trim();
    if (!name) { return; }
    if (!password) { return; }

    this.userFind = this.users.find(userSearch => userSearch.name === name);
    if (this.userFind !== undefined) {
      this.toastService.toastUserExists();
      return;
    }

    user.id = this.users.length + 1;
    user.name = name;
    user.password = password;

    /*
    this.userService.addUser(user).subscribe(newUser => {
      this.users.push(newUser);
    });
    */

    this.users.push(user);
    this.userService.saveData(this.users);
    this.toastService.toastAddUser();
  }
}

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

  constructor(private modalService: BsModalService) { }

  ngOnInit() {
  }

  createModal() {
    this.modalService.show(ModalContentComponent);
  }
}
