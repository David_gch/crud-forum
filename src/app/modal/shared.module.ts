import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalComponent } from './modal.component';
import { TranslateModule } from '../../../node_modules/ng2-translate';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule
  ],
  declarations: [ ModalComponent ],
  exports: [ ModalComponent ]
})
export class SharedModule { }
