import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';


@Injectable()
export class SpinnerService {
  private loadSubject = new BehaviorSubject<boolean>(false);
  load$ = this.loadSubject.asObservable();

  getLoadState(): Observable<boolean> {
    return this.load$;
  }

  setLoadState(load: boolean) {
    this.loadSubject.next(load);
  }
}
