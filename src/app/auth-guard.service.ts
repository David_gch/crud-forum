import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, Router } from '@angular/router';
import { LoginService } from './login/login.service';
import { ToastService } from './toast.service';

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {

  constructor(private loginService: LoginService, private router: Router, private toastService: ToastService) {}

  canActivate() {
    if (!this.loginService.isLogged()) {
      this.toastService.toastDeniedAccess();
    }
    return this.loginService.isLogged();
  }

  canActivateChild() {
    return this.loginService.isAlreadyLogged();
  }
}
